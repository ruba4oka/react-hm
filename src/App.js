import './App.scss';
import Button from "./components/Button/Button";
import React, {Component} from "react";
import Modal from "./components/Modal/Modal";

class App extends Component {

    constructor(props) {
        super(props);
        this.state = {modalState: [{id: 1, className: 'hide'}, {id: 2, className: 'hide'}]}
    }

    handleBtnClick = (btnId) => {
        this.showHideModal(btnId,'show');
    }

    handleModalBtnClick = (btnId) => {
        this.showHideModal(btnId,'hide');
    }

    showHideModal(btnId,action){
        const newState = this.state.modalState.map(el => {
            if (el.id === +btnId) {
                el.className = action;
                return el;
            } else {
                return el;
            }
        })
        this.setState({modalState: newState});
    }

    modalBtnActionsOk=(event)=>{
        this.state.modalState.find(el=>el.className==='show');
        this.showHideModal(this.state.modalState.find(el=>el.className==='show').id,'hide');
    }
    modalBtnActionsCancel=(event)=>{
        this.state.modalState.find(el=>el.className==='show');
        this.showHideModal(this.state.modalState.find(el=>el.className==='show').id,'hide');
    }

    render() {
        const btnsFirstModal = <><button className="btn-warn" onClick={this.modalBtnActionsOk}>Ок</button> <button className="btn" onClick={this.modalBtnActionsCancel}>Cancel</button></>
        const btnsSecondModal = <><button className="btn-edit"  onClick={this.modalBtnActionsOk}>Edit</button> <button className="btn" onClick={this.modalBtnActionsCancel}>Cancel</button></>
        return (
            <div className="App">
                <Button id="1" text="Open first modal" backgroundColor={"#e74c3c"} onClick={this.handleBtnClick}/>
                <Button id="2" text="Open second modal" backgroundColor={"#0B6623"} onClick={this.handleBtnClick}/>

                {this.state.modalState.find(el=>el.id===1).className==='show' &&
                < Modal id="1" header="Do you want to delete this file?" closeButton={true} text="Once you delete this file?
                it will not be possible to undo this action. Are you sure you want to delete it?" onClick={this.handleModalBtnClick} actions={btnsFirstModal} type="main"/>}

                {this.state.modalState.find(el=>el.id===2).className==='show' &&
                < Modal id="2" header="Do you want to edit this file?" closeButton={false} text="Are you sure you want to edit this file?" onClick={this.handleModalBtnClick} actions={btnsSecondModal} type="green"/>}
            </div>
        );
    }
}

export default App;
