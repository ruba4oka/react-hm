import React, {Component} from "react";
import './Button.scss'

class Button extends Component {
    render() {
        const {backgroundColor, text, onClick} = this.props;
        const color = {backgroundColor: backgroundColor}

        return (
            <button className="modalButton"  style={color} onClick={event => onClick(this.props.id)}>
                {text}
            </button>
        )
    }
}

export default Button;