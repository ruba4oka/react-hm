import React, {Component} from "react";
import './Modal.scss'

class Modal extends Component {

    closeModal = (event) => {
        console.log(event.target.className);
        if (event.target.className === 'modal modal__state_show') {
            this.props.onClick(this.props.id);
        }
    }
    closeBySvg = () => {
        this.props.onClick(this.props.id);
    }

    render() {
        const icon = (
            <svg onClick={this.closeBySvg} className="modal__close_icon" xmlns="http://www.w3.org/2000/svg"
                 height="24px" viewBox="0 0 24 24"
                 width="24px" fill="#fefefe">
                <path d="M0 0h24v24H0z" fill="none"/>
                <path
                    d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"/>
            </svg>
        )

        const {header, closeButton, text, type} = this.props;
        console.log(this.props);
        if (type === 'main') {
            return (
                <div onClick={this.closeModal} className="modal modal__state_show">
                    <div className="modal__content modal__content--main">
                        <div className="modal__header modal__header--main">
                            <h1 className="modal__title"> {header} </h1>
                            {closeButton && icon}
                        </div>
                        <div className="modal__text modal__text--main">{text} </div>
                        <div className="modal__actions modal__actions--main">
                            {this.props.actions}
                        </div>
                    </div>
                </div>
            )
        } else if(type === 'green'){
            return (
                <div onClick={this.closeModal} className="modal modal__state_show">
                    <div className="modal__content modal__content--edit">
                        <div className="modal__header modal__header--edit">
                            <h1 className="modal__title"> {header} </h1>
                            {closeButton && icon}
                        </div>
                        <div className="modal__text modal__text--edit">{text} </div>
                        <div className="modal__actions modal__actions--edit">
                            {this.props.actions}
                        </div>
                    </div>
                </div>
            )
        }
    }
}

export default Modal